# Atena
---

Atena deusa da sabedoria da mitologia grega. Este projeto ganhou este nome por conhecer os produtos favoritos dos consumidores, [documentacao do swagger](http://localhost:8080/swagger-ui.html)

## Tech
---

- Java 12
- Spring boot (cache)
- Spring Cloud (Feign, Hystrix)
- MongoDB
- Redis
- Gradle
- Atena

### Test

- JUnit 5 (unidade)
- Karate (API)
- WireMock (Mock dependencia externa)