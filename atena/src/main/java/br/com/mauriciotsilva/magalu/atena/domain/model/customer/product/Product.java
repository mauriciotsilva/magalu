package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import java.io.Serializable;
import java.util.Objects;

public class Product implements Serializable {

    private String id;
    private Price price;
    private String imageUrl;
    private String title;
    private String scoreReview;

    /**
     * @deprecated java bean
     */
    @Deprecated
    Product() {
    }

    private Product(String id, Price price, String imageUrl, String title, String scoreReview) {
        this.id = id;
        this.price = price;
        this.imageUrl = imageUrl;
        this.title = title;
        this.scoreReview = scoreReview;
    }

    public static ProductStepIdBuilder builder() {
        return new ProductBuilder();
    }


    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getScoreReview() {
        return scoreReview;
    }

    public Price getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public interface ProductStepImageUrlBuilder {
        ProductBuilder withImageUrl(String imageUrl);
    }

    public interface ProductStepPriceBuilder {
        ProductStepImageUrlBuilder withPrice(Price price);
    }

    public interface ProductStepTitleBuilder {
        ProductStepPriceBuilder withTitle(String title);
    }

    public interface ProductStepIdBuilder {
        ProductStepTitleBuilder withId(String id);
    }

    public static class ProductBuilder implements ProductStepImageUrlBuilder,
            ProductStepPriceBuilder, ProductStepTitleBuilder, ProductStepIdBuilder {

        private String id;
        private Price price;
        private String imageUrl;
        private String title;
        private String scoreReview;

        private ProductBuilder() {
        }

        public ProductBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public ProductBuilder withPrice(Price price) {
            this.price = price;
            return this;
        }

        public ProductBuilder withImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public ProductBuilder withTitle(String title) {
            this.title = title;
            return this;
        }

        public ProductBuilder withScoreReview(String scoreReview) {
            this.scoreReview = scoreReview;
            return this;
        }

        public Product build() {
            return new Product(id, price, imageUrl, title, scoreReview);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id.equals(product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
