package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import br.com.mauriciotsilva.magalu.atena.infrastructure.persistence.mongodb.FavoriteProductRepositoryCustom;
import org.springframework.data.repository.RepositoryDefinition;

@RepositoryDefinition(idClass = String.class, domainClass = FavoriteProduct.class)
public interface FavoriteProductRepository extends FavoriteProductRepositoryCustom {

}
