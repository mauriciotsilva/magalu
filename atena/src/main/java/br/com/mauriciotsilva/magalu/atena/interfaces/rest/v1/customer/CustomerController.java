package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.customer;

import br.com.mauriciotsilva.magalu.atena.interfaces.rest.RestPageableParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.ResponseEntity.*;

@RestController
@RequestMapping("customers")
public class CustomerController {

    private final CustomerFacade customerFacade;

    @Autowired
    public CustomerController(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<CustomerResponse> add(@RequestBody RegisterCustomerRequest payload, UriComponentsBuilder uriComponentsBuilder) {

        var response = customerFacade.register(payload);
        var uri = uriComponentsBuilder.pathSegment("customers", "{id}")
                .build(response.getId());

        return created(uri).body(response);
    }

    @GetMapping(value = "{id}", produces = "application/json")
    public ResponseEntity<CustomerResponse> detail(@PathVariable("id") String code) {
        return ok(customerFacade.detail(code));
    }

    @PutMapping(value = "{id}", produces = "application/json")
    public ResponseEntity<CustomerResponse> update(@PathVariable("id") String id, @RequestBody RegisterCustomerRequest request) {
        return ok(customerFacade.update(id, request));
    }

    @DeleteMapping(value = "{id}", produces = "application/json")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {

        customerFacade.remove(id);
        return noContent().build();
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<?> list(RestPageableParam pageRequest) {
        return ok(customerFacade.list(pageRequest));
    }
}
