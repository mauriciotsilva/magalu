package br.com.mauriciotsilva.magalu.atena.interfaces.rest;

import br.com.mauriciotsilva.magalu.atena.infrastructure.Translator;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.RestPageableResult;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.RestPageableResult.RestPageResultBuilder;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class PageResultTranslator {

    public <Element> RestPageableResult translate(Page<Element> page, Translator<Element, ?> translator) {
        return new RestPageResultBuilder()
                .withTotalOfElements(page.getTotalElements())
                .withNumberOfPages(page.getTotalPages())
                .withElements(page.map(translator::translate).getContent())
                .build();
    }
}
