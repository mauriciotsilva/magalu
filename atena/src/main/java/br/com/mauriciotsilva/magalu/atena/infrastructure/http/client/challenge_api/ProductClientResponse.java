package br.com.mauriciotsilva.magalu.atena.infrastructure.http.client.challenge_api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductClientResponse implements Serializable {

    public static final Long serialVersionUID = 1l;

    private String id;
    private BigDecimal price;
    private String image;
    private String brand;
    private String title;
    private String reviewScore;

    /**
     * @deprecated spring cache is using
     */
    @Deprecated
    ProductClientResponse() {
    }

    @JsonCreator
    public ProductClientResponse(
            @JsonProperty("id") String id,
            @JsonProperty("price") BigDecimal price,
            @JsonProperty("image") String image,
            @JsonProperty("brand") String brand,
            @JsonProperty("title") String title,
            @JsonProperty("reviewScore") String reviewScore) {
        this.id = id;
        this.price = price;
        this.image = image;
        this.brand = brand;
        this.title = title;
        this.reviewScore = reviewScore;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public String getBrand() {
        return brand;
    }

    public String getTitle() {
        return title;
    }

    public String getReviewScore() {
        return reviewScore;
    }

}
