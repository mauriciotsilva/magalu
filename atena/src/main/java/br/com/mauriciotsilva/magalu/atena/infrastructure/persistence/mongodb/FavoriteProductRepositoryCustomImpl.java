package br.com.mauriciotsilva.magalu.atena.infrastructure.persistence.mongodb;

import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.FavoriteProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.NoSuchElementException;

public class FavoriteProductRepositoryCustomImpl implements FavoriteProductRepositoryCustom {

    private final MongoTemplate template;

    @Autowired
    public FavoriteProductRepositoryCustomImpl(MongoTemplate template) {
        this.template = template;
    }

    @Override
    public FavoriteProduct add(FavoriteProduct favoriteProduct) {
        return template.insert(favoriteProduct);
    }

    @Override
    public boolean isFavorite(String customerId, String productId) {

        var criteria = new Criteria("customerId").is(customerId).and("product._id").is(productId);
        return template.exists(new Query(criteria), FavoriteProduct.class);
    }

    @Override
    public Page<FavoriteProduct> myProducts(String customerId, Pageable pageable) {

        var criteria = new Criteria("customerId").is(customerId);
        var results = template.find(new Query(criteria).with(pageable), FavoriteProduct.class);

        return new PageImpl<>(results, pageable, template.count(new Query(criteria), FavoriteProduct.class));
    }

    @Override
    public boolean hasProducts(String customerId) {

        var criteria = new Criteria("customerId").is(customerId);
        return template.exists(new Query(criteria), FavoriteProduct.class);
    }

    @Override
    public void remove(String customerId, String productId) {

        var criteria = new Criteria("customerId").is(customerId).and("product._id").is(productId);

        var result = template.remove(new Query(criteria), FavoriteProduct.class);
        if (result.getDeletedCount() == 0) {
            throw new NoSuchElementException("No such customer");
        }
    }

    @Override
    public void removeMyProducts(String customerId) {

        var criteria = new Criteria("customerId").is(customerId);

        var result = template.remove(new Query(criteria), FavoriteProduct.class);
        if (result.getDeletedCount() == 0) {
            throw new NoSuchElementException("No such product for customer");
        }
    }
}
