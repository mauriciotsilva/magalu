package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

@Component
public class UniqueCustomerIdentifierSpecification implements Predicate<Customer> {

    private final CustomerRepository customerRepository;

    @Autowired
    public UniqueCustomerIdentifierSpecification(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public boolean test(Customer customer) {
        return customerRepository.find(customer.getEmail()).isEmpty();
    }
}
