package br.com.mauriciotsilva.magalu.atena.interfaces.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import static org.springframework.util.Assert.isTrue;

@Component
public class PageableFactory {

    private final Integer maxPageSize;

    @Autowired
    public PageableFactory(
            @Value("${application.rest.endpoints.page.maxSize:50}") Integer maxPageSize) {
        this.maxPageSize = maxPageSize;
    }

    public Pageable create(RestPageableParam pageRequest) {

        isTrue(pageRequest.getSize() <= maxPageSize, "pageSize should be less than '" + maxPageSize + "'");

        int size = pageRequest.getSize() == 0 ? maxPageSize : pageRequest.getSize();
        return PageRequest.of(pageRequest.getPage() - 1, size);
    }
}
