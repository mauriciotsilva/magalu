package br.com.mauriciotsilva.magalu.atena.infrastructure.persistence.mongodb;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.NoSuchElementException;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class CustomerRepositoryCustomImpl implements CustomerRepositoryCustom {

    private final MongoTemplate template;

    @Autowired
    public CustomerRepositoryCustomImpl(MongoTemplate template) {
        this.template = template;
    }

    @Override
    public Customer add(Customer customer) {
        return template.insert(customer);
    }

    @Override
    public Optional<Customer> byId(String id) {
        return ofNullable(template.findById(id, Customer.class));
    }

    @Override
    public Optional<Customer> find(EmailAddress email) {

        Query query = new Query(new Criteria("email").is(email));
        return ofNullable(template.findOne(query, Customer.class));
    }

    @Override
    public Page<Customer> list(Pageable pageable) {

        var results = template.find(new Query().with(pageable), Customer.class);
        return new PageImpl<>(results, pageable, template.count(new Query(), Customer.class));
    }

    @Override
    public Customer update(Customer customer) {
        return template.save(customer);
    }

    @Override
    public void remove(String id) {

        var deleteResult = template.remove(new Query(new Criteria("_id").is(id)), Customer.class);
        if (deleteResult.getDeletedCount() == 0) {
            throw new NoSuchElementException("No such customer '" + id + "'");
        }
    }
}
