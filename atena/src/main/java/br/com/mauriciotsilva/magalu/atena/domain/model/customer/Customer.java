package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.AddingProductAsFavoriteSpecification;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.FavoriteProduct;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.InvalidValidFavoriteProductException;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.Product;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;

@TypeAlias("customer")
@Document(collection = "customers")
public class Customer {

    @Id
    private String id;
    private String name;
    private EmailAddress email;

    /**
     * @deprecated mongodb is using
     */
    @Deprecated
    Customer() {
    }

    public Customer(String name, EmailAddress email) {
        setName(name);
        setEmail(email);
    }

    public FavoriteProduct addProductAsFavorite(Product product, AddingProductAsFavoriteSpecification specification) {

        notNull(product, "product should not be nul");
        notNull(specification, "specification should not be null");

        if (!specification.test(product)) {
            throw new InvalidValidFavoriteProductException("It's not possible add this product to list");
        }

        return new FavoriteProduct(this, product);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        notNull(name, "name should not be null");
        isTrue(!name.isBlank(), "name should not be empty");
        this.name = name;
    }

    public EmailAddress getEmail() {
        return email;
    }

    public void setEmail(EmailAddress email) {
        notNull(email, "email should not be null");
        this.email = email;
    }
}
