package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

import static org.springframework.util.Assert.notNull;

@Service
public class CustomerService {

    private final Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private final CustomerRepository repository;

    @Autowired
    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public Customer toCustomer(String id) {
        notNull(id, "id should not be null");

        logger.info("Trying to retrieve customer '{}'", id);
        return repository.byId(id)
                .orElseThrow(() -> new NoSuchElementException("Customer not found"));
    }
}
