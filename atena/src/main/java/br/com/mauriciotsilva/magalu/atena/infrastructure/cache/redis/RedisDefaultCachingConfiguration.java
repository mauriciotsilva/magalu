package br.com.mauriciotsilva.magalu.atena.infrastructure.cache.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Configuration
@Profile("!test")
public class RedisDefaultCachingConfiguration extends CachingConfigurerSupport {

    private final Long defaultTllCache;

    @Autowired
    public RedisDefaultCachingConfiguration(@Value("${application.cache.ttl.default-in-seconds}") Long defaultTllCache) {
        this.defaultTllCache = defaultTllCache;
    }

    @Bean
    @ConfigurationProperties(prefix = "application.cache.ttl.time-in-seconds")
    public Map<String, Long> ttls() {
        return new HashMap<>();
    }

    @Bean
    public CacheManager cacheManager(@Qualifier("ttls") Map<String, Long> ttls, RedisConnectionFactory redisConnectionFactory) {

        Map<String, RedisCacheConfiguration> cacheConfigurations = new HashMap<>();
        ttls.forEach((name, ttl) -> cacheConfigurations.put(name, createCacheConfiguration(ttl)));

        return RedisCacheManager
                .builder(redisConnectionFactory)
                .cacheDefaults(createCacheConfiguration(defaultTllCache))
                .withInitialCacheConfigurations(cacheConfigurations).build();
    }

    private RedisCacheConfiguration createCacheConfiguration(Long timeoutInSeconds) {
        return RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofSeconds(timeoutInSeconds));
    }
}
