package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;

public class NonUniqueCustomerIdentifierException extends IllegalArgumentException {

    public NonUniqueCustomerIdentifierException(EmailAddress email) {
        super("Email '" + email + "' is already taken");
    }
}
