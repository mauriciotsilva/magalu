package br.com.mauriciotsilva.magalu.atena.application;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;

import java.util.Optional;

import static java.util.Optional.ofNullable;

public class UpdateCustomerSolicitation {

    private final String name;
    private final EmailAddress email;

    public UpdateCustomerSolicitation(String name, EmailAddress email) {
        this.name = name;
        this.email = email;
    }

    public Optional<String> getName() {
        return ofNullable(name);
    }

    public Optional<EmailAddress> getEmail() {
        return ofNullable(email);
    }
}
