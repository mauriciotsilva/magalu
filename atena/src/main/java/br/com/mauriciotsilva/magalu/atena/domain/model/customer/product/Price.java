package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import java.math.BigDecimal;

import static org.springframework.util.Assert.notNull;

public class Price {

    private final BigDecimal value;
    private final AllowedCurrency allowedCurrency;

    private Price(AllowedCurrency allowedCurrency, BigDecimal value) {
        this.allowedCurrency = allowedCurrency;
        this.value = value.setScale(allowedCurrency.getFractionDigits());
    }

    public static Price from(AllowedCurrency allowedCurrency, BigDecimal value) {

        notNull(value, "value must not be null");
        notNull(allowedCurrency, "allowed currency must not be null");

        return new Price(allowedCurrency, value);
    }

    public BigDecimal asBigDecimal() {
        return value;
    }

    @Override
    public String toString() {
        return String.join(" ", allowedCurrency.toString(), value.toString());
    }
}
