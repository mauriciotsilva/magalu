package br.com.mauriciotsilva.magalu.atena.infrastructure.http.client.challenge_api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static java.util.Optional.empty;

@Component
public class ProductClientFallback implements ProductClient {

    private final Logger logger = LoggerFactory.getLogger(ProductClientFallback.class);

    @Override
    public Optional<ProductClientResponse> fromId(String id) {

        logger.warn("It's a fallback answer for product '{}'", id);
        return empty();
    }
}
