package br.com.mauriciotsilva.magalu.atena.infrastructure.http.client.challenge_api;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(
        value = "${application.http.client.challenge_api.clients.product}",
        url = "${application.http.client.challenge_api.server}",
        fallback = ProductClientFallback.class,
        decode404 = true
)
public interface ProductClient {

    @GetMapping("api/product/{id}")
    @Cacheable(cacheNames = "products")
    Optional<ProductClientResponse> fromId(@PathVariable("id") String id);
}
