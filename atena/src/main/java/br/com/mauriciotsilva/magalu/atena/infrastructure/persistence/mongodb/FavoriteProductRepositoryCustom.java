package br.com.mauriciotsilva.magalu.atena.infrastructure.persistence.mongodb;

import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.FavoriteProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FavoriteProductRepositoryCustom {

    FavoriteProduct add(FavoriteProduct favoriteProduct);

    void remove(String customerId, String productId);

    boolean isFavorite(String customerId, String productId);

    Page<FavoriteProduct> myProducts(String customerId, Pageable pageable);

    boolean hasProducts(String customerId);

    void removeMyProducts(String customerId);
}
