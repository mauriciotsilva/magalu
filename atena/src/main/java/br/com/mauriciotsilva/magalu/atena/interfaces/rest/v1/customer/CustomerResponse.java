package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.customer;

public class CustomerResponse {

    private final String id;
    private final String name;
    private final String email;

    private CustomerResponse(String id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public static class CustomerResponseBuilder {
        private String id;
        private String name;
        private String email;

        public CustomerResponseBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public CustomerResponseBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public CustomerResponseBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public CustomerResponse build() {
            return new CustomerResponse(id, name, email);
        }
    }
}
