package br.com.mauriciotsilva.magalu.atena.infrastructure.rest.swagger;

import br.com.mauriciotsilva.magalu.atena.infrastructure.Translator;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Optional;

import static java.util.Optional.ofNullable;

@ConfigurationProperties(prefix = "application.info")
public class RestDocumentation {

    private String title;
    private String description;
    private String version;
    private Contact contact;
    private AuthorizationServerDocumentation authorization;

    public <Output> Optional<Output> mappingContact(Translator<Contact, Output> translator) {
        return ofNullable(contact).map(translator::translate);
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public AuthorizationServerDocumentation getAuthorization() {
        return authorization;
    }

    public void setAuthorization(AuthorizationServerDocumentation authorization) {
        this.authorization = authorization;
    }

    public static class Contact {

        private String name;
        private String url;
        private String email;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
