package br.com.mauriciotsilva.magalu.atena.infrastructure.persistence.mongodb;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CustomerRepositoryCustom {

    Customer add(Customer customer);

    Optional<Customer> byId(String id);

    Optional<Customer> find(EmailAddress email);

    Customer update(Customer customer);

    void remove(String code);

    Page<Customer> list(Pageable pageable);
}
