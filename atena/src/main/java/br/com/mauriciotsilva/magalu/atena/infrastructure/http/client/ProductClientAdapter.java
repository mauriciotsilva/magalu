package br.com.mauriciotsilva.magalu.atena.infrastructure.http.client;

import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.Price;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.Product;
import br.com.mauriciotsilva.magalu.atena.infrastructure.http.client.challenge_api.ProductClient;
import br.com.mauriciotsilva.magalu.atena.infrastructure.http.client.challenge_api.ProductClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.AllowedCurrency.BRL;

@Component
public class ProductClientAdapter {

    private final Logger logger = LoggerFactory.getLogger(ProductClientAdapter.class);
    private final ProductClient client;

    @Autowired
    public ProductClientAdapter(ProductClient client) {
        this.client = client;
    }

    public Optional<Product> fromProductId(String id) {

        logger.info("Retrieving product from external system with id '{}'", id);
        return client.fromId(id).map(this::toProduct);
    }

    private Product toProduct(ProductClientResponse clientResponse) {
        logger.trace("Adapting product '{}' from external system to Application", clientResponse.getId());

        return Product.builder()
                .withId(clientResponse.getId())
                .withTitle(clientResponse.getTitle())
                .withPrice(Price.from(BRL, clientResponse.getPrice()))
                .withImageUrl(clientResponse.getImage())
                .build();
    }
}
