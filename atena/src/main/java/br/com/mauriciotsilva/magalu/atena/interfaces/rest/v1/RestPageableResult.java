package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1;

import java.util.List;

public class RestPageableResult {

    private final int numberOfPages;
    private final long totalOfElements;
    private final List<?> elements;

    private RestPageableResult(int numberOfPages, long totalOfElements, List<?> elements) {
        this.numberOfPages = numberOfPages;
        this.totalOfElements = totalOfElements;
        this.elements = elements;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public long getTotalOfElements() {
        return totalOfElements;
    }

    public List<?> getElements() {
        return elements;
    }

    public static class RestPageResultBuilder {

        private int numberOfPages;
        private long totalOfElements;
        private List<?> elements;

        public RestPageResultBuilder withNumberOfPages(int numberOfPages) {
            this.numberOfPages = numberOfPages;
            return this;
        }

        public RestPageResultBuilder withTotalOfElements(long totalOfElements) {
            this.totalOfElements = totalOfElements;
            return this;
        }

        public RestPageResultBuilder withElements(List<?> elements) {
            this.elements = elements;
            return this;
        }

        public RestPageableResult build() {
            return new RestPageableResult(numberOfPages, totalOfElements, elements);
        }
    }
}
