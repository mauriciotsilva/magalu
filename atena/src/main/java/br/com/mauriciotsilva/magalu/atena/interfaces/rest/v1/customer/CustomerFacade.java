package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.customer;

import br.com.mauriciotsilva.magalu.atena.application.CustomerApplication;
import br.com.mauriciotsilva.magalu.atena.application.UpdateCustomerSolicitation;
import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.Customer;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.PageResultTranslator;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.PageableFactory;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.RestPageableParam;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.RestPageableResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerFacade {

    private final PageResultTranslator pageResultTranslator;
    private final CustomerApplication customerApplication;
    private final PageableFactory pageableFactory;

    @Autowired
    public CustomerFacade(
            PageableFactory pageableFactory, PageResultTranslator pageResultTranslator,
            CustomerApplication customerApplication) {
        this.pageableFactory = pageableFactory;
        this.pageResultTranslator = pageResultTranslator;
        this.customerApplication = customerApplication;
    }

    public CustomerResponse register(RegisterCustomerRequest request) {
        return customerApplication.add(request.getName(), EmailAddress.from(request.getEmail()), this::toResponse);
    }

    public CustomerResponse detail(String id) {
        return customerApplication.detail(id, this::toResponse);
    }

    public RestPageableResult list(RestPageableParam pageRequest) {
        return customerApplication.list(
                pageableFactory.create(pageRequest),
                page -> pageResultTranslator.translate(page, this::toResponse)
        );
    }

    public CustomerResponse update(String id, RegisterCustomerRequest request) {

        var solicitation = new UpdateCustomerSolicitation(request.getName(), EmailAddress.from(request.getEmail()));
        return customerApplication.update(id, solicitation, this::toResponse);
    }

    public void remove(String id) {
        customerApplication.remove(id);
    }

    private CustomerResponse toResponse(Customer customer) {
        return new CustomerResponse.CustomerResponseBuilder()
                .withId(customer.getId())
                .withName(customer.getName())
                .withEmail(customer.getEmail().toString())
                .build();
    }
}
