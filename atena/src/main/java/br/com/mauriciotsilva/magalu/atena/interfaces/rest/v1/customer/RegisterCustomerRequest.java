package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.customer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterCustomerRequest {

    private final String name;
    private final String email;

    @JsonCreator
    public RegisterCustomerRequest(
            @JsonProperty("name") String name,
            @JsonProperty("email") String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
