package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import br.com.mauriciotsilva.magalu.atena.infrastructure.persistence.mongodb.CustomerRepositoryCustom;
import org.springframework.data.repository.RepositoryDefinition;

@RepositoryDefinition(domainClass = Customer.class, idClass = String.class)
public interface CustomerRepository extends CustomerRepositoryCustom {
}
