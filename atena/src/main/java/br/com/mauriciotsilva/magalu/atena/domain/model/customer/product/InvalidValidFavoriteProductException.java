package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

public class InvalidValidFavoriteProductException extends IllegalArgumentException {

    public InvalidValidFavoriteProductException(String message) {
        super(message);
    }
}
