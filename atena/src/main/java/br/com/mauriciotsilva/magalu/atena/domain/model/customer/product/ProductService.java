package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import br.com.mauriciotsilva.magalu.atena.infrastructure.http.client.ProductClientAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

import static org.springframework.util.Assert.notNull;

@Service
public class ProductService {

    private final Logger logger = LoggerFactory.getLogger(ProductService.class);
    private final ProductClientAdapter clientAdapter;

    @Autowired
    public ProductService(ProductClientAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Product toProduct(String id) {
        notNull(id, "product id should not be null");

        logger.info("Trying to retrieve product '{}'", id);
        return clientAdapter
                .fromProductId(id)
                .orElseThrow(() -> new NoSuchElementException("Product not found"));
    }
}
