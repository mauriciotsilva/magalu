package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import br.com.mauriciotsilva.magalu.atena.domain.model.customer.Customer;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.function.Consumer;

import static org.springframework.util.Assert.notNull;

@TypeAlias("productAsFavorite")
@Document(collection = "favorites")
public class FavoriteProduct {

    @Id
    private String id;

    private String customerId;
    private Product product;

    /**
     * @deprecated mongodb is using
     */
    @Deprecated
    FavoriteProduct() {
    }

    public FavoriteProduct(Customer customer, Product product) {
        notNull(customer, "customer should not be null");

        setCustomerId(customer.getId());
        setProduct(product);
    }

    public void describeProduct(Consumer<Product> consumer) {
        consumer.accept(product);
    }

    public String getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {

        notNull(product, "product should not be null");
        this.product = product;
    }

    public void setCustomerId(String customerId) {

        notNull(customerId, "customerId should not be null");
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }
}
