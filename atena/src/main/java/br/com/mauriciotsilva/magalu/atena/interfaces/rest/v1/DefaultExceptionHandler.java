package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;
import static org.springframework.web.util.WebUtils.ERROR_EXCEPTION_ATTRIBUTE;

@ControllerAdvice
public class DefaultExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> internalServerError(Exception exception) {
        return createResponse(exception, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<?> badRequestError(RuntimeException exception) {
        return createResponse(exception, BAD_REQUEST);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<?> notFoundError(NoSuchElementException exception) {
        return createResponse(exception, NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {

        if (INTERNAL_SERVER_ERROR == status) {
            request.setAttribute(ERROR_EXCEPTION_ATTRIBUTE, ex, SCOPE_REQUEST);
        }

        return createResponse(ex, status);
    }

    private ResponseEntity<Object> createResponse(Exception ex, HttpStatus status) {
        return ResponseEntity.status(status).body(new RestErrorResponse(ex.getMessage()));
    }
}
