package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import java.util.Currency;

public enum AllowedCurrency {

    BRL;

    public int getFractionDigits() {
        return getCurrency().getDefaultFractionDigits();
    }

    private Currency getCurrency() {
        return Currency.getInstance(toString());
    }
}
