package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1;

public class RestErrorResponse {

    private final String message;

    public RestErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
