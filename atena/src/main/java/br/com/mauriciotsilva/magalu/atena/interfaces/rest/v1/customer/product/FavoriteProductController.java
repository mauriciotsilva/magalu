package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.customer.product;

import br.com.mauriciotsilva.magalu.atena.interfaces.rest.RestPageableParam;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.RestPageableResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("customers/{customerId}/favorites")
public class FavoriteProductController {

    private final FavoriteProductFacade favoriteProductFacade;

    @Autowired
    public FavoriteProductController(FavoriteProductFacade favoriteProductFacade) {
        this.favoriteProductFacade = favoriteProductFacade;
    }


    @PostMapping(value = "{productId}", produces = "application/json")
    public ResponseEntity<?> add(
            @PathVariable("customerId") String customerId,
            @PathVariable("productId") String productId) {
        return addImpl(customerId, productId);
    }

    @PutMapping(value = "{productId}", produces = "application/json")
    public ResponseEntity<?> put(
            @PathVariable("customerId") String customerId,
            @PathVariable("productId") String productId) {
        return addImpl(customerId, productId);
    }

    private ResponseEntity<?> addImpl(String customerId, String productId) {
        favoriteProductFacade.addProductAsFavorite(customerId, productId);
        return noContent().build();
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<RestPageableResult> list(
            @PathVariable("customerId") String customerId, RestPageableParam pageableParam) {
        return ok(favoriteProductFacade.listFavorites(customerId, pageableParam));
    }

    @DeleteMapping(value = "{productId}", produces = "application/json")
    public ResponseEntity<?> remove(
            @PathVariable("customerId") String customerId,
            @PathVariable("productId") String productId) {

        favoriteProductFacade.removeProductAsFavorite(customerId, productId);
        return noContent().build();
    }
}
