package br.com.mauriciotsilva.magalu.atena.interfaces.rest;

import static org.springframework.util.Assert.isTrue;

public class RestPageableParam {

    private int page = 1;
    private int size;

    /**
     * @deprecated java bean
     */
    @Deprecated
    RestPageableParam() {
    }

    RestPageableParam(int page, int size) {
        setPage(page);
        setSize(size);
    }

    public void setPage(int page) {
        isTrue(page > 0, "page should be greater than '0'");
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
