package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import br.com.mauriciotsilva.magalu.atena.domain.model.customer.Customer;

import java.util.function.Predicate;

public class AddingProductAsFavoriteSpecification implements Predicate<Product> {

    private final Customer customer;
    private final FavoriteProductRepository favorites;

    public AddingProductAsFavoriteSpecification(Customer customer, FavoriteProductRepository favorites) {
        this.customer = customer;
        this.favorites = favorites;
    }

    @Override
    public boolean test(Product product) {
        return !favorites.isFavorite(customer.getId(), product.getId());
    }
}
