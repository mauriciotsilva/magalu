package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.customer.product;

import br.com.mauriciotsilva.magalu.atena.application.CustomerApplication;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.FavoriteProduct;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.PageResultTranslator;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.PageableFactory;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.RestPageableParam;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.RestPageableResult;
import br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.customer.product.FavoriteProductResponse.FavoriteProductResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FavoriteProductFacade {

    private final CustomerApplication customerApplication;
    private final PageResultTranslator pageResultTranslator;
    private final PageableFactory pageableFactory;

    @Autowired
    public FavoriteProductFacade(
            CustomerApplication customerApplication, PageResultTranslator pageResultTranslator,
            PageableFactory pageableFactory) {
        this.customerApplication = customerApplication;
        this.pageResultTranslator = pageResultTranslator;
        this.pageableFactory = pageableFactory;
    }

    public void addProductAsFavorite(String customerId, String productId) {
        customerApplication.addProductInFavoriteList(customerId, productId);
    }

    public RestPageableResult listFavorites(String customerId, RestPageableParam pageParam) {
        return customerApplication.myFavorites(
                customerId, pageableFactory.create(pageParam),
                page -> pageResultTranslator.translate(page, this::toResponse)
        );
    }

    public void removeProductAsFavorite(String customerId, String productId) {
        customerApplication.removeProductFromFavoriteList(customerId, productId);
    }

    public FavoriteProductResponse toResponse(FavoriteProduct favoriteProduct) {

        var builder = new FavoriteProductResponseBuilder();
        favoriteProduct.describeProduct(product ->
                builder.withTitle(product.getTitle())
                        .withImage(product.getImageUrl())
                        .withId(product.getId())
                        .withPrice(product.getPrice().asBigDecimal())
                        .withScoreReview(product.getScoreReview())
        );

        return builder.build();
    }
}
