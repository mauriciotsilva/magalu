package br.com.mauriciotsilva.magalu.atena.infrastructure;

public interface Translator<Type, Output> {

    Output translate(Type type);
}
