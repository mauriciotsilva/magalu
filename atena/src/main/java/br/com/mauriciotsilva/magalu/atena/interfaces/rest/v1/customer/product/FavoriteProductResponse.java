package br.com.mauriciotsilva.magalu.atena.interfaces.rest.v1.customer.product;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
public class FavoriteProductResponse {

    private final String title;
    private final String image;
    private final BigDecimal price;
    private final String id;
    private final String scoreReview;

    private FavoriteProductResponse(String title, String image, BigDecimal price, String id, String scoreReview) {
        this.title = title;
        this.image = image;
        this.price = price;
        this.id = id;
        this.scoreReview = scoreReview;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getScoreReview() {
        return scoreReview;
    }

    public static class FavoriteProductResponseBuilder {

        private String title;
        private String image;
        private BigDecimal price;
        private String id;
        private String scoreReview;

        public FavoriteProductResponseBuilder withTitle(String title) {
            this.title = title;
            return this;
        }

        public FavoriteProductResponseBuilder withImage(String image) {
            this.image = image;
            return this;
        }

        public FavoriteProductResponseBuilder withPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public FavoriteProductResponseBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public FavoriteProductResponseBuilder withScoreReview(String scoreReview) {
            this.scoreReview = scoreReview;
            return this;
        }

        public FavoriteProductResponse build() {
            return new FavoriteProductResponse(title, image, price, id, scoreReview);
        }
    }
}
