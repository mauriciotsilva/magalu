package br.com.mauriciotsilva.magalu.atena.infrastructure.rest.swagger;

import br.com.mauriciotsilva.magalu.atena.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Profile("!test")
@Configuration
@EnableConfigurationProperties(RestDocumentation.class)
public class RestDocumentConfiguration {

    public static final String AUTHORIZATION_SERVER = "Authorization server";

    private final RestDocumentation documentation;

    @Autowired
    public RestDocumentConfiguration(RestDocumentation documentation) {
        this.documentation = documentation;
    }

    @Bean
    public Docket api() {
        return new Docket(SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(Application.class.getPackageName()))
                .build()
                .securitySchemes(List.of(securitySchema(documentation.getAuthorization())))
                .apiInfo(apiInfo(documentation))
                .securityContexts(List.of(securityContext(documentation.getAuthorization())));
    }

    @Bean
    public SecurityConfiguration securityInfo() {
        var authorization = documentation.getAuthorization();
        return SecurityConfigurationBuilder.builder()
                .clientId(authorization.getClient())
                .clientSecret(authorization.getSecret())
                .scopeSeparator(" ")
                .useBasicAuthenticationWithAccessCodeGrant(false)
                .build();
    }

    private SecurityContext securityContext(AuthorizationServerDocumentation documentation) {
        return SecurityContext.builder()
                .securityReferences(defaultAuth(documentation))
                .forPaths(PathSelectors.any())
                .build();
    }

    private List<SecurityReference> defaultAuth(AuthorizationServerDocumentation documentation) {

        var scopes = documentation.getScopes()
                .stream().map(scope -> new AuthorizationScope(scope.getName(), scope.getDescription()))
                .toArray(AuthorizationScope[]::new);

        return List.of(new SecurityReference(AUTHORIZATION_SERVER, scopes));
    }

    private OAuth securitySchema(AuthorizationServerDocumentation documentation) {

        var scopes = documentation.getScopes()
                .stream().map(scope -> new AuthorizationScope(scope.getName(), scope.getDescription()))
                .collect(toList());

        return new OAuth(AUTHORIZATION_SERVER, scopes, List.of(new ResourceOwnerPasswordCredentialsGrant(documentation.getServer())));
    }

    private ApiInfo apiInfo(RestDocumentation documentation) {

        var apiInfo = new ApiInfoBuilder()
                .title(documentation.getTitle())
                .description(documentation.getDescription())
                .version(documentation.getVersion());

        documentation
                .mappingContact(contact -> new Contact(contact.getName(), contact.getUrl(), contact.getEmail()))
                .ifPresent(apiInfo::contact);

        return apiInfo.build();
    }
}
