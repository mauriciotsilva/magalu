package br.com.mauriciotsilva.magalu.atena.application;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.*;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.AddingProductAsFavoriteSpecification;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.FavoriteProduct;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.FavoriteProductRepository;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.ProductService;
import br.com.mauriciotsilva.magalu.atena.infrastructure.Translator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static org.springframework.util.Assert.notNull;

@Service
public class CustomerApplication {

    private final Logger logger = LoggerFactory.getLogger(CustomerApplication.class);

    private final CustomerRepository customers;
    private final CustomerService customerService;
    private final ProductService productService;
    private final FavoriteProductRepository favorites;
    private final UniqueCustomerIdentifierSpecification specification;

    @Autowired
    public CustomerApplication(
            CustomerRepository customers, CustomerService customerService, ProductService productService,
            FavoriteProductRepository favorites, UniqueCustomerIdentifierSpecification specification) {
        this.customers = customers;
        this.customerService = customerService;
        this.productService = productService;
        this.favorites = favorites;
        this.specification = specification;
    }

    public <Output> Output add(String name, EmailAddress email, Translator<Customer, Output> translator) {

        notNull(translator, "translator must not be null");

        var customer = new Customer(name, email);
        if (!specification.test(customer)) {
            logger.warn("Email '{}' already taken", email);
            throw new NonUniqueCustomerIdentifierException(email);
        }

        logger.info("Trying to store customer '{}' with email '{}'", customer.getId(), email);
        return translator.translate(customers.add(customer));
    }

    public <Output> Output update(String id, UpdateCustomerSolicitation solicitation, Translator<Customer, Output> translator) {

        notNull(translator, "translator must not be null");
        notNull(solicitation, "solicitation must not be null");

        var customer = customerService.toCustomer(id);
        var name = solicitation.getName();
        var email = solicitation.getEmail();

        if (name.isPresent() || email.isPresent()) {
            name.ifPresent(customer::setName);
            email.ifPresent(customer::setEmail);

            logger.info("Updating customer with id: '{}'", customer.getId());
            return translator.translate(customers.update(customer));
        }

        return translator.translate(customer);
    }

    public <Output> Output detail(String id, Translator<Customer, Output> translator) {
        var customer = customerService.toCustomer(id);
        logger.info("Retrieving details from customer with id: '{}'", customer.getId());

        return translator.translate(customer);
    }

    public void remove(String id) {
        customers.remove(id);
        if (favorites.hasProducts(id)) {
            favorites.removeMyProducts(id);
        }
    }

    public <Output> Output list(Pageable pageable, Translator<Page<Customer>, Output> translator) {

        var result = customers.list(pageable);
        logger.info("Listing all customers, {} results", result.getTotalElements());

        return translator.translate(result);
    }

    public void addProductInFavoriteList(String customerId, String productId) {

        var customer = customerService.toCustomer(customerId);
        var product = productService.toProduct(productId);

        var specification = new AddingProductAsFavoriteSpecification(customer, favorites);
        favorites.add(customer.addProductAsFavorite(product, specification));

        logger.info("Product '{}' added as favorite in customer '{}' list", product.getId(), customer.getId());
    }

    public void removeProductFromFavoriteList(String customerId, String productId) {

        logger.info("Trying to remove product '{}' from customer '{}' list", productId, customerId);
        favorites.remove(customerId, productId);
    }

    public <Output> Output myFavorites(String customerId, Pageable pageable, Translator<Page<FavoriteProduct>, Output> translator) {

        var products = favorites.myProducts(customerId, pageable);
        logger.info("Listing favorites '{}' products from customer '{}'", products.getNumberOfElements(), customerId);

        return translator.translate(products);
    }
}
