package br.com.mauriciotsilva.magalu.atena.domain.model;

import java.util.Objects;

import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;

public class EmailAddress {

    private final static String VALID_EMAIL_REGEX = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    private final String value;

    private EmailAddress(String value) {
        this.value = value;
    }

    public static EmailAddress from(String value) {
        notNull(value, "email must not be null");
        isTrue(value.matches(VALID_EMAIL_REGEX), "RFC 5322 - invalid email format");

        return new EmailAddress(value);
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailAddress that = (EmailAddress) o;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
