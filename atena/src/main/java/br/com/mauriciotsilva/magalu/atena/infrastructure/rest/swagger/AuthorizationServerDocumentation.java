package br.com.mauriciotsilva.magalu.atena.infrastructure.rest.swagger;

import java.util.ArrayList;
import java.util.List;

public class AuthorizationServerDocumentation {

    private String server;
    private String client;
    private String secret;
    private List<ScopeDetail> scopes = new ArrayList<>();

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public List<ScopeDetail> getScopes() {
        return scopes;
    }

    public void setScopes(List<ScopeDetail> scopes) {
        this.scopes = scopes;
    }

    public static class ScopeDetail {

        private String name;
        private String description;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

}
