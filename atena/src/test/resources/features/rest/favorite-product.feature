Feature: Product as favorite

  Background:
    Given def customer = {name: "Juan", email: "juan@email.com"}
    And callonce read("util/add-customer.feature") customer
    And url host
    And path "customers", response.id, "favorites"

  Scenario: should not add product as favorite when it does not exits

    Given path "INVALID_PRODUCT_ID"
    And request {}
    When method post
    Then status 400
    And print "response: ", response

  Scenario: should add product as favorite in my list

    Given path "VALID_PRODUCT_ID"
    And request {}
    When method post
    Then status 204

  Scenario: should not add product as favorite when it is already listed
    Given path "VALID_PRODUCT_ID"
    And request {}
    When method post
    Then status 400
    And print "response: ", response
