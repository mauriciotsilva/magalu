Feature: Customers

  Background:
    Given url host
    And path "customers"

  Scenario: should add a customer

    Given def customer = {name: "Paulo", email: "paulo@email.com"}
    When call read("util/add-customer.feature") customer
    Then match response == {id: "#present", name: "#(name)", email: "#(email)"}
    And match header Location contains response.id

  Scenario: should update customer

    * def newCustomerEmail = "ricardo.pereira@email.com";

    Given def customer = {name: "Ricardo Pereira", email: "ricardo@email.com"}
    And call read("util/add-customer.feature") customer
    And set customer.email = newCustomerEmail
    When path response.id
    And request customer
    And method put
    Then status 200
    And match response.email == newCustomerEmail

  Scenario: should view customer details
    Given def customer = {name: "Olivia", email: "olivia@email.com"}
    And call read("util/add-customer.feature") customer
    When path response.id
    And method get
    Then status 200
    And match response == {name: "#(customer.name)", email: "#(customer.email)", id: "#string"}

  Scenario: should remove a customer
    Given def customer = {name: "Marta", email: "marta@email.com"}
    And call read("util/add-customer.feature") customer
    When path response.id
    And method delete
    Then status 204

  Scenario Outline: should not add a customer with invalid data

    Given request {name: "<name>", email: "<email>"}
    When method post
    Then status 400

    Examples:
      | name        | email             | descricao          |
      |             |                   | Sem nome e email   |
      | Marcos      |                   | Sem email          |
      |             | marcela@email.com | sem nome           |
      | Bianca Lima | bianca.lima       | com email invalido |

  Scenario Outline: should not add a customer with the same email address
    Given request {name: "<name>", email: "<email>"}
    When method post
    Then status <status>

    Examples:
      | name    | email             | status |
      | Marcelo | marcelo@email.com | 201    |
      | Joao    | marcelo@email.com | 400    |



