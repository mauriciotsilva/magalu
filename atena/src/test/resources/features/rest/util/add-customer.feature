@ignore
Feature:

  Background:
    Given url host
    And path "customers"

  Scenario: should add a customer
    Given request {name: "#(name)", email: "#(email)"}
    When method post
    Then status 201
    And match response == {id: "#present", name: "#(name)", email: "#(email)"}
    And match header Location contains response.id