function(){

    autoConfiguration({
        timeout: 5000,
        prettyPrint: true
    });

    return {
        host: "http://localhost:" + karate.properties["serverPort"]
    };
}


function autoConfiguration(configuration){

    var config = {
        prettyPrint: function(){
            karate.configure("logPrettyRequest", configuration.prettyPrint);
            karate.configure("logPrettyResponse", configuration.prettyPrint);

            return this;
        },
        timeout: function(){
            karate.configure("readTimeout", configuration.timeout);
            karate.configure("connectTimeout", configuration.timeout);

            return this;
        }
    };

    return config.prettyPrint().timeout();

}

