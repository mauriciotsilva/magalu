package br.com.mauriciotsilva.magalu.atena;

import com.intuit.karate.junit5.Karate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
@EnableAutoConfiguration(exclude = {RedisAutoConfiguration.class})
public class KarateFeatureRunner {

    @LocalServerPort
    private Integer serverPort;

    @BeforeEach
    public void setup() {
        System.setProperty("serverPort", String.valueOf(serverPort));
    }

    @Karate.Test
    @DisplayName("should execute customers endpoint")
    public Karate testCustomer() {
        return createKarate("classpath:features/rest/customers.feature");
    }

    @Karate.Test
    @DisplayName("should execute product as favorite endpoint")
    public Karate testProductAsFavorite() {
        return createKarate("classpath:features/rest/favorite-product.feature");
    }

    private Karate createKarate(String feature) {
        return new Karate()
                .feature(feature)
                .relativeTo(getClass());
    }
}