package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PriceTest {

    private BigDecimal value = BigDecimal.ONE;
    private AllowedCurrency allowedCurrency = AllowedCurrency.BRL;
    private Price subject;

    @BeforeEach
    public void setup() {
        subject = Price.from(allowedCurrency, value);
    }

    @Test
    @DisplayName("should not create price when allowed currency is null")
    public void testWithNullAllowedCurrency() {
        assertThrows(
                IllegalArgumentException.class,
                () -> Price.from(null, value),
                "should throws exception when allowed currency is null"
        );
    }

    @Test
    @DisplayName("should not create price when value is null")
    public void testWithNullValue() {
        assertThrows(
                IllegalArgumentException.class,
                () -> Price.from(allowedCurrency, null),
                "should throws exception when value is null"
        );
    }

    @Test
    @DisplayName("should have scale 2 for currency BRL")
    public void testValue() {
        var value = subject.asBigDecimal();
        assertEquals(new BigDecimal(1).setScale(2), value, "scale should be 2 for BRL");
    }

    @Test
    @DisplayName("should print toString as BRL 1.00")
    public void testToString() {
        assertEquals("BRL 1.00", subject.toString(), "should be BRL plus value with 2 decimal numbers as cents");
    }
}