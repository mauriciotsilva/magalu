package br.com.mauriciotsilva.magalu.atena.application;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.*;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.*;
import br.com.mauriciotsilva.magalu.atena.infrastructure.Translator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerApplicationTest {

    private static final String CUSTOMER_NAME = "CUSTOMER_NAME";
    private static final String CUSTOMER_ID = "CUSTOMER_ID";
    private static final String PRODUCT_ID = "PRODUCT_ID";

    @Mock
    private CustomerRepository customers;
    @Mock
    private CustomerService customerService;
    @Mock
    private ProductService productService;
    @Mock
    private FavoriteProductRepository favorites;
    @Mock
    private UniqueCustomerIdentifierSpecification specification;

    private CustomerApplication subject;

    @Mock
    private UpdateCustomerSolicitation solicitation;
    @Mock
    private EmailAddress email;
    @Mock
    private Customer customer;
    @Mock
    private Pageable pageable;
    @Captor
    private ArgumentCaptor<Customer> captor;

    private Translator<Customer, ?> translator;

    @BeforeEach
    public void setup() {

        translator = identity()::apply;
        lenient().when(specification.test(any(Customer.class))).thenReturn(true);
        lenient().when(customerService.toCustomer(CUSTOMER_ID)).thenReturn(customer);

        subject = new CustomerApplication(customers, customerService, productService, favorites, specification);
    }

    @Test
    @DisplayName("should add customer")
    public void testAdd() {

        subject.add(CUSTOMER_NAME, email, translator);
        verify(customers).add(captor.capture());

        var customer = captor.getValue();
        assertEquals(CUSTOMER_NAME, customer.getName());
        assertEquals(email, customer.getEmail());
    }

    @Test
    @DisplayName("should valid all required fields on adding customer")
    public void testAllRequiredFieldsOnAdding() {
        assertThrows(
                IllegalArgumentException.class,
                () -> subject.add(CUSTOMER_NAME, email, null),
                "translator should not be null"
        );
    }

    @Test
    @DisplayName("should throw exception when email already exist")
    public void testNonUniqueSpecification() {

        when(specification.test(any(Customer.class))).thenReturn(false);
        assertThrows(
                NonUniqueCustomerIdentifierException.class,
                () -> subject.add(CUSTOMER_NAME, email, translator),
                "should throw exception when spec not valid"
        );
    }

    @Test
    @DisplayName("should update customer")
    public void testUpdate() {

        when(solicitation.getEmail()).thenReturn(ofNullable(email));

        subject.update(CUSTOMER_ID, solicitation, translator);
        verify(customers).update(customer);
    }

    @Test
    @DisplayName("should not update when data is not given")
    public void testUpdateWhenDataIsNotGiven() {

        subject.update(CUSTOMER_ID, solicitation, translator);
        verify(customers, never()).update(customer);
    }

    @Test
    @DisplayName("should valid all required fields on updating customer")
    public void testValidFieldsUpdate() {

        var exception = IllegalArgumentException.class;
        assertAll("all required fields on update",
                () -> assertThrows(exception, () -> subject.update(CUSTOMER_ID, null, translator), "solicitation should not be null"),
                () -> assertThrows(exception, () -> subject.update(CUSTOMER_ID, solicitation, null), "translator should not be null")
        );
    }

    @Test
    @DisplayName("should retrieve customer detail")
    public void testDetail() {

        var result = subject.detail(CUSTOMER_ID, translator);
        assertEquals(customer, result);
    }

    @Test
    @DisplayName("should remove customer but not remove favorites when there is nothing")
    public void testRemoveWhenHasNotProducts() {

        when(favorites.hasProducts(CUSTOMER_ID)).thenReturn(false);

        subject.remove(CUSTOMER_ID);

        verify(customers).remove(CUSTOMER_ID);
        verify(favorites, never()).removeMyProducts(CUSTOMER_ID);
    }

    @Test
    @DisplayName("should remove customer and its favorites product")
    public void testRemove() {

        when(favorites.hasProducts(CUSTOMER_ID)).thenReturn(true);

        subject.remove(CUSTOMER_ID);

        verify(customers).remove(CUSTOMER_ID);
        verify(favorites).removeMyProducts(CUSTOMER_ID);
    }

    @Test
    @DisplayName("should list customer")
    public void testList() {

        when(customers.list(pageable)).thenReturn(mock(Page.class));

        subject.list(pageable, identity()::apply);
        verify(customers).list(pageable);
    }

    @Test
    @DisplayName("should list my favorites products")
    public void testMyFavorites() {

        when(favorites.myProducts(CUSTOMER_ID, pageable)).thenReturn(mock(Page.class));

        subject.myFavorites(CUSTOMER_ID, pageable, identity()::apply);
        verify(favorites).myProducts(CUSTOMER_ID, pageable);
    }

    @Test
    @DisplayName("should remove product as favorite")
    public void testRemoveFavoriteProduct() {

        subject.removeProductFromFavoriteList(CUSTOMER_ID, PRODUCT_ID);
        verify(favorites).remove(CUSTOMER_ID, PRODUCT_ID);
    }

    @Test
    @DisplayName("should add product as favorite")
    public void testAddProductAsFavorite() {

        var product = mock(Product.class);
        var favorite = mock(FavoriteProduct.class);

        when(productService.toProduct(PRODUCT_ID)).thenReturn(product);
        when(customerService.toCustomer(CUSTOMER_ID)).thenReturn(customer);
        when(customer.addProductAsFavorite(eq(product), any(AddingProductAsFavoriteSpecification.class))).thenReturn(favorite);

        subject.addProductInFavoriteList(CUSTOMER_ID, PRODUCT_ID);
        verify(favorites).add(favorite);
    }
}