package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.AddingProductAsFavoriteSpecification;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.FavoriteProductRepository;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AddingProductAsFavoriteSpecificationTest {

    public static final String CUSTOMER_ID = "SOME_CUSTOMER_ID";
    public static final String PRODUCT_ID = "SOME_PRODUCT_ID";
    @Mock
    private FavoriteProductRepository favorites;
    @Mock
    private Customer customer;

    private AddingProductAsFavoriteSpecification subject;

    @Mock
    private Product product;

    @BeforeEach
    public void setup() {

        lenient().when(product.getId()).thenReturn(PRODUCT_ID);
        lenient().when(customer.getId()).thenReturn(CUSTOMER_ID);
        lenient().when(favorites.isFavorite(CUSTOMER_ID, PRODUCT_ID)).thenReturn(false);

        subject = new AddingProductAsFavoriteSpecification(customer, favorites);
    }

    @Test
    @DisplayName("should be valid when product is not listed")
    public void testValidSpecification() {

        var result = subject.test(product);
        assertTrue(result, "product out of list");
    }

    @Test
    @DisplayName("should be invalid when product is listed")
    public void testInvalidSpecification() {

        when(favorites.isFavorite(eq(CUSTOMER_ID), any(String.class))).thenReturn(true);

        var result = subject.test(product);
        assertFalse(result, "product already listed");
    }
}