package br.com.mauriciotsilva.magalu.atena.domain.model.customer.product;

import br.com.mauriciotsilva.magalu.atena.infrastructure.http.client.ProductClientAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.NoSuchElementException;

import static java.util.Optional.empty;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    private static final String SOME_PRODUCT_ID = "SOME_PRODUCT_ID";

    @Mock
    private ProductClientAdapter clientAdapter;

    private ProductService subject;

    @BeforeEach
    public void setup() {
        subject = new ProductService(clientAdapter);
    }

    @Test
    @DisplayName("should not be null product id")
    public void testNullable() {
        assertThrows(IllegalArgumentException.class, () -> subject.toProduct(null), "should not be null");
    }

    @Test
    @DisplayName("should not return product when id is not found")
    public void testWhenProductIsEmpty() {

        when(clientAdapter.fromProductId(SOME_PRODUCT_ID)).thenReturn(empty());
        assertThrows(NoSuchElementException.class, () -> subject.toProduct(SOME_PRODUCT_ID), "should throw exception when id not found");
    }
}