package br.com.mauriciotsilva.magalu.atena.domain.model;

import br.com.mauriciotsilva.magalu.atena.domain.model.customer.Customer;
import br.com.mauriciotsilva.magalu.atena.infrastructure.persistence.mongodb.CustomerRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InMemoryCustomerRepository implements CustomerRepositoryCustom {
    @Override
    public Customer add(Customer customer) {
        return null;
    }

    @Override
    public Optional<Customer> byId(String id) {
        return Optional.empty();
    }

    @Override
    public Optional<Customer> find(EmailAddress email) {
        return Optional.empty();
    }

    @Override
    public Customer update(Customer customer) {
        return null;
    }

    @Override
    public void remove(String code) {

    }

    @Override
    public Page<Customer> list(Pageable pageable) {
        return null;
    }
}
