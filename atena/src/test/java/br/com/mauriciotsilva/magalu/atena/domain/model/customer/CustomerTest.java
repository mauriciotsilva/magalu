package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.AddingProductAsFavoriteSpecification;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.InvalidValidFavoriteProductException;
import br.com.mauriciotsilva.magalu.atena.domain.model.customer.product.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerTest {

    public static final String SOME_EMAIL = "SOME_EMAIL";
    public static final String SOME_BLANK_NAME = " ";
    public static final String SOME_CUSTOMER_ID = "SOME_CUSTOMER_ID";

    @Mock
    private EmailAddress email;
    @Mock
    private Product product;
    @Mock
    private AddingProductAsFavoriteSpecification specification;

    private Customer subject;


    @BeforeEach
    public void setup() {

        lenient().when(specification.test(product)).thenReturn(true);

        subject = spy(new Customer(SOME_EMAIL, email));
    }

    @Test
    @DisplayName("should add valid product as favorite")
    public void testAddProductAsFavorite() {

        when(subject.getId()).thenReturn(SOME_CUSTOMER_ID);

        var result = subject.addProductAsFavorite(product, specification);

        assertEquals(SOME_CUSTOMER_ID, result.getCustomerId(), "should add customer id");
        assertEquals(product, result.getProduct(), "should add product");
    }

    @Test
    @DisplayName("should not add an invalid product as favorite")
    public void testValidationAddProductAsFavorite() {

        var invalidArgument = IllegalArgumentException.class;
        var invalidFavoriteProduct = InvalidValidFavoriteProductException.class;

        Executable whenSpecificationIsFalse = () -> {
            when(specification.test(product)).thenReturn(false);
            subject.addProductAsFavorite(product, specification);
        };

        assertAll("all product as favorite constraints",
                () -> assertThrows(invalidArgument, () -> subject.addProductAsFavorite(null, specification), "product should not be null"),
                () -> assertThrows(invalidArgument, () -> subject.addProductAsFavorite(product, null), "spec should not be null"),
                () -> assertThrows(invalidFavoriteProduct, whenSpecificationIsFalse, "should throw exception when spec is false")
        );
    }

    @Test
    @DisplayName("should not create customer when required fields are null")
    public void testAllRequiredFields() {

        var exception = IllegalArgumentException.class;
        assertAll("required fields",
                () -> assertThrows(exception, () -> new Customer(SOME_BLANK_NAME, email), "name should not be empty"),
                () -> assertThrows(exception, () -> new Customer(null, email), "name should not be null"),
                () -> assertThrows(exception, () -> new Customer(SOME_EMAIL, null), "email should not be null")
        );
    }
}