package br.com.mauriciotsilva.magalu.atena.domain.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmailAddressTest {

    private static final String INVALID_EMAIL_ADDRESS = "someone";
    private static final String VALID_EMAIL_ADDRESS = "someone@server.com";

    private EmailAddress subject;

    @BeforeEach
    public void setup() {
        subject = EmailAddress.from(VALID_EMAIL_ADDRESS);
    }

    @Test
    @DisplayName("should not create when email is null")
    public void testWithNullEmail() {
        assertThrows(
                IllegalArgumentException.class,
                () -> EmailAddress.from(null),
                "email must not be null"
        );
    }

    @Test
    @DisplayName("should not create when email is invalid")
    public void testWithInvalidEmail() {
        assertThrows(
                IllegalArgumentException.class,
                () -> EmailAddress.from(INVALID_EMAIL_ADDRESS)
        );
    }

    @Test
    @DisplayName("should print toString with email")
    public void testToString() {
        assertEquals(VALID_EMAIL_ADDRESS, subject.toString());
    }
}