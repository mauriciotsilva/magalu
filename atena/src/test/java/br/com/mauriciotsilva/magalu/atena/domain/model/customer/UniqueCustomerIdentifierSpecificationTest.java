package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import br.com.mauriciotsilva.magalu.atena.domain.model.EmailAddress;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UniqueCustomerIdentifierSpecificationTest {

    @Mock
    private CustomerRepository customerRepository;

    private UniqueCustomerIdentifierSpecification subject;

    @Mock
    private EmailAddress email;
    @Mock
    private EmailAddress nonRegisteredEmail;
    @Mock
    private Customer customer;

    @BeforeEach
    public void setup() {

        lenient().when(customer.getEmail()).thenReturn(email);
        lenient().when(customerRepository.find(email)).thenReturn(empty());

        subject = new UniqueCustomerIdentifierSpecification(customerRepository);
    }

    @Test
    @DisplayName("should be a valid specification when email does not exist")
    public void testValidSpecification() {

        var result = subject.test(customer);
        assertTrue(result, "should be valid");
    }

    @Test
    @DisplayName("should be an invalid specification when email already exists")
    public void testInvalidSpecification() {

        when(customer.getEmail()).thenReturn(nonRegisteredEmail);
        when(customerRepository.find(nonRegisteredEmail)).thenReturn(ofNullable(customer));

        var result = subject.test(customer);
        assertFalse(result, "should be invalid");
    }
}