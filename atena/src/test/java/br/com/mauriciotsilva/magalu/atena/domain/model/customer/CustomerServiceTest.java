package br.com.mauriciotsilva.magalu.atena.domain.model.customer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.NoSuchElementException;
import java.util.UUID;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    private static final String CUSTOMER_CODE = UUID.randomUUID().toString();
    private static final String NOT_FOUND_CUSTOMER_CODE = UUID.randomUUID().toString();

    @Mock
    private Customer customer;
    @Mock
    private CustomerRepository customerRepository;

    private CustomerService subject;

    @BeforeEach
    public void setup() {

        lenient().when(customerRepository.byId(CUSTOMER_CODE)).thenReturn(ofNullable(customer));

        subject = new CustomerService(customerRepository);
    }

    @Test
    @DisplayName("should retrieve customer from code")
    public void test() {

        var result = subject.toCustomer(CUSTOMER_CODE);
        assertEquals(customer, result, "should return customer");
    }

    @Test
    @DisplayName("should not retrieve when code is null or not found")
    public void testToCustomer() {

        Executable whenProductNotFound = () -> {
            when(customerRepository.byId(NOT_FOUND_CUSTOMER_CODE)).thenReturn(empty());
            subject.toCustomer(NOT_FOUND_CUSTOMER_CODE);
        };

        assertAll("Retrieve customer",
                () -> assertThrows(IllegalArgumentException.class, () -> subject.toCustomer(null), "code should not be null"),
                () -> assertThrows(NoSuchElementException.class, whenProductNotFound, "code not found")
        );
    }
}