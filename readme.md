# Magalu
---

A solucao foi idealizada para usar um conjunto de boas praticas presentes no mercado e tenologias atuais, todos os componentes da solucao foram pensado para enderecar problemas reais em ambientes reais!

## Projetos

- **Asgard:** [API Gateway](/asgard/readme.md) que guarda os endpoints de dominio e o _authentication server_
- **Heimdall:** [Authentication server](/heimdall/readme.md) para fazer a autenticacao e autorizacao com _OAuth2_
- **Atena:** [Consumer service](/atena/readme.md) servico que contem o dominio (baseado DDD e arquitetura hexagonal) e os endpoints

## Infraestrutura

- Docker
- Docker compose

## Executando a Aplicacao

Para executar a aplicacao basta executar o seguinte comando:
```shell
$ ./gradlew clean build && docker-compose up -d
```
> A primera execucao pode ser mais lenta, pois tanto o Docker quanto os testes precisam preparar o ambiente
