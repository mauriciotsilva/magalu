package br.com.mauriciotsilva.magalu.asgard.infrastructure.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

import static org.springframework.http.HttpMethod.*;


@Configuration
public class ResourceAuthorizationConfigurer extends ResourceServerConfigurerAdapter {

    private final String authorizationServer;
    private final String clientId;
    private final String clientSecret;

    @Autowired
    public ResourceAuthorizationConfigurer(
            @Value("${authorization.oauth.server}") String authorizationServer,
            @Value("${authorization.oauth.clientId}") String clientId,
            @Value("${authorization.oauth.clientSecret}") String clientSecret) {
        this.authorizationServer = authorizationServer;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {

        RemoteTokenServices remoteTokenServices = new RemoteTokenServices();
        remoteTokenServices.setCheckTokenEndpointUrl(authorizationServer);
        remoteTokenServices.setClientId(clientId);
        remoteTokenServices.setClientSecret(clientSecret);

        resources.tokenServices(remoteTokenServices);
    }

    @Override
    public void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/v2/api-docs", "/swagger-resources/**", "/swagger-ui.html", "/webjars/**", "/api/v2/api-docs", "/api/oauth/**").permitAll()
                .antMatchers(POST, "/api/customers").access("#oauth2.hasScope('customer::add')")
                .antMatchers(PUT, "/api/customers/*").access(" #oauth2.hasScope('customer::update')")
                .antMatchers(GET, "/api/customers/*", "/api/customers").authenticated()
                .antMatchers(DELETE, "/api/customers/*").access("hasAuthority('ADMIN') and #oauth2.hasScope('customer::remove')")
                .antMatchers(POST, "/api/customers/*/favorite/*").access("#oauth2.hasScope('customer::favorite::add')")
                .antMatchers(PUT, "/api/customers/*/favorite/*").access("#oauth2.hasScope('customer::favorite::add')")
                .antMatchers(DELETE, "/api/customers/*/favorite/*").access("#oauth2.hasScope('customer::favorite::remove')")
                .anyRequest()
                .authenticated();
    }

}