# Asgard
---

Mundo dos deuses na mitologia nordica, [documentacao do swagger](http://localhost:7070/swagger-ui.html)

## Permissionamento de URL

| Path                                  |  Method       | Escopo                        | Authority |
|------------------                     |---------      |---------------                |-----------|
|   `/api/customers`                    |   POST        |   customer::add               |      *    |
|   `/api/customers/*`                  |   PUT         |   customer::update            |      *    |
|   `/api/customers`                    |   GET         |   Nenhum                      |      *    |
|   `/api/customers/*`                  |   GET         |   Nenhum                      |      *    |
|   `/api/customers/*`                  |   DELETE      |   customer::remove            |   ADMIN   |
|   `/api/customers/*/favorite/*`       |   POST        |   customer::favorite::add     |      *    |
|   `/api/customers/*/favorite/*`       |   PUT         |   customer::favorite::add     |      *    |
|   `/api/customers/*/favorite/*`       |   DELETE      |   customer::favorite::remove  |      *    |

## Tech 
---

- Java 12
- Spring Boot
- Spring Security (OAuth2)
- Spring Cloud (Netflix Zuul - API Gateway)
- Gradle
- Docker