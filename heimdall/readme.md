# Heimdall
---

Deus da mitogia nordica e guardiao de Bifrost, porta de entrada de Asgard. Por este motivo o **_Authorization server_** possui este nome.

## Credenciais

Todas as credenciais estao armazenas em **memoria**

### Usuarios

|   username    |   password    |   authority   |
|----           |----           |-----          |
|   admin       |   admin       |   ADMIN       |
|   magalu      |   magalu      |   USER        |

### Applications (Clients)

|   client  |   secret  | grant type        |   escopo                      |
|----       |---        |---                |---                            |
|   client  |   secret  |   password        |   customer::add               |
|           |           |                   |   customer::remove            |
|           |           |                   |   customer::update            |
|           |           |                   |   customer::favorite::add     |
|           |           |                   |   customer::favorite::remove  |

## Tech
---

- Java 12
- Spring boot (security, oauth2)
- Gradle
- Docker