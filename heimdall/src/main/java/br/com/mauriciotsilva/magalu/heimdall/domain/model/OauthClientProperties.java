package br.com.mauriciotsilva.magalu.heimdall.domain.model;

import java.util.ArrayList;
import java.util.List;

public class OauthClientProperties {

    private String client;
    private String secret;
    private List<String> scopes = new ArrayList<>();
    private List<String> grantTypes = new ArrayList<>();
    private Long accessTokenExpirationTimeInMinutes;

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }

    public List<String> getGrantTypes() {
        return grantTypes;
    }

    public void setGrantTypes(List<String> grantTypes) {
        this.grantTypes = grantTypes;
    }

    public Long getAccessTokenExpirationTimeInMinutes() {
        return accessTokenExpirationTimeInMinutes;
    }

    public void setAccessTokenExpirationTimeInMinutes(Long accessTokenExpirationTimeInMinutes) {
        this.accessTokenExpirationTimeInMinutes = accessTokenExpirationTimeInMinutes;
    }
}
