package br.com.mauriciotsilva.magalu.heimdall.infrastructure.security;

import br.com.mauriciotsilva.magalu.heimdall.domain.model.OauthCredentialConfigure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

@Configuration
public class DefaultAuthorizationServer implements AuthorizationServerConfigurer {

    private final OauthCredentialConfigure oauthCredentialConfigure;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public DefaultAuthorizationServer(OauthCredentialConfigure oauthCredentialConfigure, AuthenticationManager authenticationManager) {
        this.oauthCredentialConfigure = oauthCredentialConfigure;
        this.authenticationManager = authenticationManager;
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return oauthCredentialConfigure.toUserDetailsService();
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security.tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()")
                .passwordEncoder(NoOpPasswordEncoder.getInstance());
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) {
        oauthCredentialConfigure.configure(clients);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService());
    }
}