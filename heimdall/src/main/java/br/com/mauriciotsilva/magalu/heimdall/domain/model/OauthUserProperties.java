package br.com.mauriciotsilva.magalu.heimdall.domain.model;

import java.util.ArrayList;
import java.util.List;

public class OauthUserProperties {

    private String username;
    private String password;
    private List<String> authorities = new ArrayList<>();

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
