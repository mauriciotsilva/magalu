package br.com.mauriciotsilva.magalu.heimdall.domain.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class OauthCredentialConfigure {

    private final OauthCredentialProperties oauthCredential;

    @Autowired
    public OauthCredentialConfigure(OauthCredentialProperties oauthCredential) {
        this.oauthCredential = oauthCredential;
    }

    public UserDetailsService toUserDetailsService() {

        var details = oauthCredential.getUsers().values().stream()
                .map(this::toUserDetail)
                .toArray(UserDetails[]::new);

        return new InMemoryUserDetailsManager(details);
    }

    private UserDetails toUserDetail(OauthUserProperties properties) {
        return User.withDefaultPasswordEncoder()
                .username(properties.getUsername())
                .password(properties.getPassword())
                .authorities(properties.getAuthorities().toArray(String[]::new))
                .build();
    }

    public void configure(ClientDetailsServiceConfigurer clients) {
        oauthCredential.getClients().values().forEach(client -> addOauthClient(clients, client));
    }

    private void addOauthClient(ClientDetailsServiceConfigurer clients, OauthClientProperties oauthClient) {

        try {

            clients.inMemory()
                    .withClient(oauthClient.getClient())
                    .secret(oauthClient.getSecret())
                    .scopes(oauthClient.getScopes().toArray(String[]::new))
                    .resourceIds()
                    .authorizedGrantTypes(oauthClient.getGrantTypes().toArray(String[]::new))
                    .autoApprove(true)
                    .accessTokenValiditySeconds(toSeconds(Duration.ofMinutes(oauthClient.getAccessTokenExpirationTimeInMinutes())));
        } catch (Exception e) {
            throw new RuntimeException("It's not possible configure OauthClient", e);
        }
    }

    private int toSeconds(Duration duration) {
        return (int) duration.toSeconds();
    }
}
