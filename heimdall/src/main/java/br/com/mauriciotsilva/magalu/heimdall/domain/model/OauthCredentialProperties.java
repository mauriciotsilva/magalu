package br.com.mauriciotsilva.magalu.heimdall.domain.model;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

@ConfigurationProperties("ouath.credentials")
public class OauthCredentialProperties {

    private Map<String, OauthUserProperties> users = new HashMap<>();
    private Map<String, OauthClientProperties> clients = new HashMap<>();

    public Map<String, OauthUserProperties> getUsers() {
        return users;
    }

    public void setUsers(Map<String, OauthUserProperties> users) {
        this.users = users;
    }

    public Map<String, OauthClientProperties> getClients() {
        return clients;
    }

    public void setClients(Map<String, OauthClientProperties> clients) {
        this.clients = clients;
    }
}
